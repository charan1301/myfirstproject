package com.kgisl.nsure.mail.config;

import java.util.Properties;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.JavaMailSenderImpl;

import com.kgisl.nsure.mail.dao.MailConfigDAO;
import com.kgisl.nsure.mail.model.MailConfigDO;

@Configuration
@ComponentScan(basePackages = "com.kgisl.nsure")
public class MailConfig {

	private static final Logger logger = Logger.getLogger("mail");
	
	@Autowired
	private MailConfigDAO mailConfigDAO;

	@Bean
	public JavaMailSender getMailSender() {
		JavaMailSenderImpl mailSender = null;
		try {
			mailSender = new JavaMailSenderImpl();
			MailConfigDO configDO = mailConfigDAO.getMailServerInfo();
			if (configDO != null) {
				mailSender.setHost(configDO.getHost());
				mailSender.setPort(configDO.getPort());
				mailSender.setUsername(configDO.getMailSender());

				Properties javaMailProperties = new Properties();
				javaMailProperties.put("mail.smtp.auth", configDO.getSmtpAuth());
				javaMailProperties.put("mail.debug", configDO.getMailDebug());

				mailSender.setJavaMailProperties(javaMailProperties);
			}
		} catch (Exception e) {
			logger.info("Exception while reading mail server information." + e.toString());
		}
		return mailSender;
	}

}
