package com.kgisl.nsure.mail.dao;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Map;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Repository;

import com.kgisl.nsure.mail.model.MailConfigDO;

@Repository(value="mailConfigDAO")
public class MailConfigDAO {

	private static final Logger logger = Logger.getLogger("mail");
	
	@Autowired
	private NamedParameterJdbcTemplate namedParameterJdbcTemplate;
	
	public MailConfigDO getMailServerInfo() {
		MailConfigDO configDO = null;
		try{
			Map<String, String> params = null;
			configDO = namedParameterJdbcTemplate.queryForObject(MailConstants.MAIL_SERVER_INFO, params, new RowMapper<MailConfigDO>() {
				@Override
				public MailConfigDO mapRow(ResultSet rs, int rowNum) throws SQLException {
					MailConfigDO configDO = new MailConfigDO();
					configDO.setHost(rs.getString("HOST"));
					configDO.setPort(rs.getInt("PORT"));
					configDO.setMailDebug(rs.getString("MAILDEBUG"));
					configDO.setSmtpAuth(rs.getString("SMTPAUTH"));
					configDO.setMailSender(rs.getString("SENDER"));
					return configDO;
				}
			});
		}
		catch (EmptyResultDataAccessException e) {}
		catch (Exception e) {
			logger.info("Exception while reading mail server details :"+e.toString());
		}
		return configDO;
	}

	public MailConfigDO getEmailDetails(final MailConfigDO mailConfigDO,int emailCategoryId) {
		try{
			String query = "";
			MapSqlParameterSource params = new MapSqlParameterSource();
			params.addValue("emailCatId", emailCategoryId);
			
			query = "SELECT * FROM ( SELECT VCH_FROM_MAIL_ID,VCH_TO_MAIL_ID,VCH_CC_MAIL_ID,VCH_BCC_MAIL_ID,VCH_MAIL_SUBJECT,VCH_MAIL_CONTENT FROM EIS_MST_OUTGOING_EMAIL_ID"+
					" WHERE NUM_EMAIL_CATEGORY_ID = :emailCatId AND CHR_ACTIVE_STATUS = 'Y' ORDER BY DTT_CREATION_DATE DESC)WHERE ROWNUM=1";
			
			namedParameterJdbcTemplate.queryForObject(query, params,new RowMapper<MailConfigDO>() {
				@Override
				public MailConfigDO mapRow(ResultSet rs, int rowNum) throws SQLException {
					mailConfigDO.setFromAddress(rs.getString("VCH_FROM_MAIL_ID"));
					if(mailConfigDO.getToAddress()==null || (mailConfigDO.getToAddress()!=null && mailConfigDO.getToAddress().trim().length()==0))
						mailConfigDO.setToAddress(rs.getString("VCH_TO_MAIL_ID"));
					mailConfigDO.setCcAddress(rs.getString("VCH_CC_MAIL_ID"));
					mailConfigDO.setBccAddress(rs.getString("VCH_BCC_MAIL_ID"));
					mailConfigDO.setMailSubject(rs.getString("VCH_MAIL_SUBJECT"));
					if(mailConfigDO.getMailContent()==null || (mailConfigDO.getMailContent()!=null && mailConfigDO.getMailContent().trim().length()==0))
						mailConfigDO.setMailContent(rs.getString("VCH_MAIL_CONTENT"));
					return mailConfigDO;
				}
			});
		}
		catch(EmptyResultDataAccessException empty){}
		catch(Exception e){
			logger.info("Exception while getting email details :"+e.toString());
		}
		return mailConfigDO;
	}
	
}

final class MailConstants {
	final static String MAIL_SERVER_INFO = new StringBuilder("SELECT ")
			.append(" (SELECT A.VCH_CONFIG_VALUE FROM EIS_MST_DOMAIN_CONFIG A WHERE A.VCH_CONFIG_NAME = 'mail.smtp.port') PORT,")
			.append(" (SELECT A.VCH_CONFIG_VALUE FROM EIS_MST_DOMAIN_CONFIG A WHERE A.VCH_CONFIG_NAME = 'mail.smtp.host') HOST,")
			.append(" (SELECT A.VCH_CONFIG_VALUE FROM EIS_MST_DOMAIN_CONFIG A WHERE A.VCH_CONFIG_NAME = 'mail.sender') SENDER,")
			.append(" (SELECT A.VCH_CONFIG_VALUE FROM EIS_MST_DOMAIN_CONFIG A WHERE A.VCH_CONFIG_NAME = 'mail.smtp.auth') SMTPAUTH,")
			.append(" (SELECT A.VCH_CONFIG_VALUE FROM EIS_MST_DOMAIN_CONFIG A WHERE A.VCH_CONFIG_NAME = 'mail.debug') MAILDEBUG")
			.append(" FROM DUAL")
			.toString()
			;
}
