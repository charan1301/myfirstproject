package com.kgisl.nsure.mail.model;

public enum AddressType {
	TO, CC, BCC
}
