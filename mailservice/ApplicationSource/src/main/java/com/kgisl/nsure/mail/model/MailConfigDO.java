package com.kgisl.nsure.mail.model;

import java.io.Serializable;
import java.util.Map;

@SuppressWarnings("serial")
public class MailConfigDO implements Serializable {

	// Mail configuration
	private String host;
	private int port;
	private String smtpAuth;
	private String mailDebug;
	private String mailSender;

	private String fromAddress;
	private String toAddress;
	private String ccAddress;
	private String bccAddress;
	private String mailSubject;
	private String mailContent;
	private MailType mailType;
	private Map<String, String> fileNames;
	private Map<String, byte[]> attachments;
	
	//Non Motor Mail Config
	private String coverNoteNo;
	private String referenceNo;
	private String quotationNo;
	private String mainClassDesc;
	private String subClassCode;
	private String subClassDesc;
	private String agentCode;
	private String agentName;
	private String insuredName;
	private String inceptionDate;
	private String expiryDate;
	private String insuredMailId;
	private String agentMailId;
	private String staffMailId;
	private String housePhNo;
	private String officePhNo;
	private String mobileNo;
	private String faxNo;
	private String cancelRemarks;
	private String referRiskRemarks;
	private String approvalRemarks;

	public String getHost() {
		return host;
	}

	public void setHost(String host) {
		this.host = host;
	}

	public int getPort() {
		return port;
	}

	public void setPort(int port) {
		this.port = port;
	}

	public String getSmtpAuth() {
		return smtpAuth;
	}

	public void setSmtpAuth(String smtpAuth) {
		this.smtpAuth = smtpAuth;
	}

	public String getMailDebug() {
		return mailDebug;
	}

	public void setMailDebug(String mailDebug) {
		this.mailDebug = mailDebug;
	}

	public String getMailSender() {
		return mailSender;
	}

	public void setMailSender(String mailSender) {
		this.mailSender = mailSender;
	}

	public String getFromAddress() {
		return fromAddress;
	}

	public void setFromAddress(String fromAddress) {
		this.fromAddress = fromAddress;
	}

	public String getToAddress() {
		return toAddress;
	}

	public void setToAddress(String toAddress) {
		this.toAddress = toAddress;
	}

	public String getCcAddress() {
		return ccAddress;
	}

	public void setCcAddress(String ccAddress) {
		this.ccAddress = ccAddress;
	}

	public String getBccAddress() {
		return bccAddress;
	}

	public void setBccAddress(String bccAddress) {
		this.bccAddress = bccAddress;
	}

	public String getMailSubject() {
		return mailSubject;
	}

	public void setMailSubject(String mailSubject) {
		this.mailSubject = mailSubject;
	}

	public String getMailContent() {
		return mailContent;
	}

	public void setMailContent(String mailContent) {
		this.mailContent = mailContent;
	}

	public MailType getMailType() {
		return mailType;
	}

	public void setMailType(MailType mailType) {
		this.mailType = mailType;
	}

	public Map<String, String> getFileNames() {
		return fileNames;
	}

	public void setFileNames(Map<String, String> fileNames) {
		this.fileNames = fileNames;
	}

	public Map<String, byte[]> getAttachments() {
		return attachments;
	}

	public void setAttachments(Map<String, byte[]> attachments) {
		this.attachments = attachments;
	}

	public String getCoverNoteNo() {
		return coverNoteNo;
	}

	public void setCoverNoteNo(String coverNoteNo) {
		this.coverNoteNo = coverNoteNo;
	}

	public String getReferenceNo() {
		return referenceNo;
	}

	public void setReferenceNo(String referenceNo) {
		this.referenceNo = referenceNo;
	}

	public String getQuotationNo() {
		return quotationNo;
	}

	public void setQuotationNo(String quotationNo) {
		this.quotationNo = quotationNo;
	}

	public String getMainClassDesc() {
		return mainClassDesc;
	}

	public void setMainClassDesc(String mainClassDesc) {
		this.mainClassDesc = mainClassDesc;
	}

	public String getSubClassCode() {
		return subClassCode;
	}

	public void setSubClassCode(String subClassCode) {
		this.subClassCode = subClassCode;
	}

	public String getSubClassDesc() {
		return subClassDesc;
	}

	public void setSubClassDesc(String subClassDesc) {
		this.subClassDesc = subClassDesc;
	}

	public String getAgentCode() {
		return agentCode;
	}

	public void setAgentCode(String agentCode) {
		this.agentCode = agentCode;
	}

	public String getAgentName() {
		return agentName;
	}

	public void setAgentName(String agentName) {
		this.agentName = agentName;
	}

	public String getInsuredName() {
		return insuredName;
	}

	public void setInsuredName(String insuredName) {
		this.insuredName = insuredName;
	}

	public String getInceptionDate() {
		return inceptionDate;
	}

	public void setInceptionDate(String inceptionDate) {
		this.inceptionDate = inceptionDate;
	}

	public String getExpiryDate() {
		return expiryDate;
	}

	public void setExpiryDate(String expiryDate) {
		this.expiryDate = expiryDate;
	}

	public String getInsuredMailId() {
		return insuredMailId;
	}

	public void setInsuredMailId(String insuredMailId) {
		this.insuredMailId = insuredMailId;
	}

	public String getAgentMailId() {
		return agentMailId;
	}

	public void setAgentMailId(String agentMailId) {
		this.agentMailId = agentMailId;
	}
	
	public String getStaffMailId() {
		return staffMailId;
	}

	public void setStaffMailId(String staffMailId) {
		this.staffMailId = staffMailId;
	}

	public String getHousePhNo() {
		return housePhNo;
	}

	public void setHousePhNo(String housePhNo) {
		this.housePhNo = housePhNo;
	}

	public String getOfficePhNo() {
		return officePhNo;
	}

	public void setOfficePhNo(String officePhNo) {
		this.officePhNo = officePhNo;
	}

	public String getMobileNo() {
		return mobileNo;
	}

	public void setMobileNo(String mobileNo) {
		this.mobileNo = mobileNo;
	}

	public String getFaxNo() {
		return faxNo;
	}

	public void setFaxNo(String faxNo) {
		this.faxNo = faxNo;
	}

	public String getCancelRemarks() {
		return cancelRemarks;
	}

	public void setCancelRemarks(String cancelRemarks) {
		this.cancelRemarks = cancelRemarks;
	}

	public String getReferRiskRemarks() {
		return referRiskRemarks;
	}

	public void setReferRiskRemarks(String referRiskRemarks) {
		this.referRiskRemarks = referRiskRemarks;
	}

	public String getApprovalRemarks() {
		return approvalRemarks;
	}

	public void setApprovalRemarks(String approvalRemarks) {
		this.approvalRemarks = approvalRemarks;
	}
}
