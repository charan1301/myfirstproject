package com.kgisl.nsure.mail.model;

public enum MailType {

	PLAIN("text/plain"), IMAGE("image/jpeg"), HTML("text/html"), MULTIPART("multipart/mixed");

	private String value;

	@Override
	public String toString() {
		return this.value;
	}

	private MailType(String value) {
		this.value = value;
	}

}
