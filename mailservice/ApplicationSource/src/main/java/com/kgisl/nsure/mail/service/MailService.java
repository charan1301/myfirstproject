package com.kgisl.nsure.mail.service;

import com.kgisl.nsure.mail.model.MailConfigDO;

public interface MailService {

	public void sendMail(final MailConfigDO mailConfigDO);
	
	public void sendMailByCategory(final MailConfigDO mailConfigDO,int emailCategoryId);

}
