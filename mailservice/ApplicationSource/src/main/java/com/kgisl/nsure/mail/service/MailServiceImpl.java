/***************************************************************************************************************
 * Date			Developer Name			Defect							
 * 
 * 21-NOV-2018	Asaithambi				DF: 4881 - Mail Service: Check the email address before creating the thread
 * 20-DEC-2018	Asaithambi				DF: 5032 - Error Captured in the logs
 * 
 ****************************************************************************************************************/

package com.kgisl.nsure.mail.service;

import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.mail.javamail.MimeMessagePreparator;
import org.springframework.stereotype.Service;

import com.kgisl.nsure.mail.dao.MailConfigDAO;
import com.kgisl.nsure.mail.model.AddressType;
import com.kgisl.nsure.mail.model.MailConfigDO;
import com.kgisl.nsure.mail.model.MailType;

@Service("mailService")
public class MailServiceImpl implements MailService {

	private static final Logger logger = Logger.getLogger("mail");
	
	@Autowired
	private JavaMailSender mailSender;

	@Autowired
	private MailConfigDAO mailConfigDAO;
	
	public static final int EMAIL_CAT_NM_REFER_RISK_ROUTE = 35;
	public static final int EMAIL_CAT_NM_REFER_RISK_APPROVED = 36;
	public static final int EMAIL_CAT_NM_REFER_RISK_REJECTED = 37;
	public static final int EMAIL_CAT_NM_QT_REFER_RISK_ROUTE = 38;
	public static final int EMAIL_CAT_NM_QT_REFER_RISK_APPROVED = 39;
	public static final int EMAIL_CAT_NM_QT_REFER_RISK_REJECTED = 40;	
	public static final int EMAIL_CAT_NM_CANCELLATION_ROUTE = 41;
	public static final int EMAIL_CAT_NM_CANCELLATION_APPROVED = 42;
	public static final int EMAIL_CAT_NM_CANCELLATION_REJECTED = 43;

	// DF: 4881 [S]
	private static final String EMAIL_REGEX = "^[\\w-\\+]+(\\.[\\w]+)*@[\\w-]+(\\.[\\w]+)*(\\.[a-z]{2,})$";
	private static Pattern pattern = Pattern.compile(EMAIL_REGEX, Pattern.CASE_INSENSITIVE);
	private static Matcher matcher;
	// DF: 4881 [E]
	
	@Override
	public void sendMail(final MailConfigDO mailConfigDO) {
		try {
			
			logger.info("\n----------- Mail Sending process starts for ("+mailConfigDO.getMailSubject()+")--------");
			
			// Calling email message preparator.
			final MimeMessagePreparator preparator = getMessagePreparator(mailConfigDO);
			
			logger.info("From Address	:"+mailConfigDO.getFromAddress());
			logger.info("To Address	:"+mailConfigDO.getToAddress());
			logger.info("CC Address	:"+mailConfigDO.getCcAddress());
			logger.info("BCC Address	:"+mailConfigDO.getBccAddress());
			logger.info("Mail subject	:"+mailConfigDO.getMailSubject());
			logger.info("Mail Type	:"+mailConfigDO.getMailType());
			
			if(preparator != null && mailConfigDO.getFromAddress() != null 
					&& !mailConfigDO.getFromAddress().trim().equalsIgnoreCase("NA")) {
				
				// Thread to send email to the recipient.
				Runnable mailThread = new Runnable() {
					@Override
					public void run() {
						mailSender.send(preparator);
					}
				};
				mailThread.run();
			}
			
		} catch (Exception e) {
			logger.info("Exception while sending mail ... " + e.toString());
		}
		
		logger.info("----------- Mail Sending process ends for ("+mailConfigDO.getMailSubject()+")--------");
	}

	/**
	 * Core method for email preparing with the details received from the mailConfigDO
	 * @param mailConfigDO - passing mailing information.
	 * @return MimeMessagePreparator
	 */
	private MimeMessagePreparator getMessagePreparator(final MailConfigDO mailConfigDO) {
		// DF: 4881 [S]
		return new MimeMessagePreparator() {
			public void prepare(MimeMessage mimeMessage) throws Exception {
				MimeMessageHelper messageHelper = new MimeMessageHelper(mimeMessage, true, "UTF-8");

				if(!(mailConfigDO.getFromAddress() != null && mailConfigDO.getFromAddress().trim().length() > 0)){
					MailConfigDO fromAddress = mailConfigDAO.getMailServerInfo();
					if(fromAddress != null) {
						mailConfigDO.setFromAddress(fromAddress.getMailSender());
					}
				}
				
				// Return null when from address received as null or invalid.
				if(!(mailConfigDO.getFromAddress() != null && mailConfigDO.getFromAddress().trim().length() > 0 
						&& isEmail(mailConfigDO.getFromAddress()))) {
					mailConfigDO.setFromAddress("NA");
				} else {
					messageHelper.setFrom(new InternetAddress(mailConfigDO.getFromAddress()));
					
					// TO ADDRESS VALIDATION
					String toMails = filterInvalidEmails(mailConfigDO.getToAddress());
					if(toMails != null && toMails.trim().length() > 0 ) {
						mailConfigDO.setToAddress(toMails);
					} else {
						mailConfigDO.setToAddress(null);
						mailConfigDO.setFromAddress("NA");
					}
					
					// CC ADDRESS VALIDATION
					mailConfigDO.setCcAddress(filterInvalidEmails(mailConfigDO.getCcAddress()));
					// BCC ADDRESS VALIDATION
					mailConfigDO.setBccAddress(filterInvalidEmails(mailConfigDO.getBccAddress()));
					
					// ALLOW TO PROCEED ONLY WHEN FROM & TO ADRESS IS VALID.
					if(mailConfigDO.getFromAddress() != null && !mailConfigDO.getFromAddress().trim().equalsIgnoreCase("NA")) {
						// TO ADDRESS
						prepareInternetAddress(messageHelper, mailConfigDO.getToAddress(), AddressType.TO);
						// CC ADDRESS
						prepareInternetAddress(messageHelper, mailConfigDO.getCcAddress(), AddressType.CC);
						// BCC ADDRESS
						prepareInternetAddress(messageHelper, mailConfigDO.getBccAddress(), AddressType.BCC);
						
						messageHelper.setSubject(mailConfigDO.getMailSubject());
						if(mailConfigDO.getMailType() == MailType.MULTIPART) {
							messageHelper.setText(mailConfigDO.getMailContent());
						} else if(mailConfigDO.getMailType() == MailType.HTML) {
							messageHelper.setText(mailConfigDO.getMailContent(),mailConfigDO.getMailContent());
						} else {
							messageHelper.setText(mailConfigDO.getMailContent());
						}
					    
						prepareMailAttachment(messageHelper, mailConfigDO);
					}
				}
				
			}
		};
	}
	
	/**
	 * To Filter Invalid Email addresses. 
	 * @param emails
	 * @return
	 */
	private static String filterInvalidEmails(String emails) {
		String validEmails = null;
		try{
			if(emails != null && emails.trim().length() > 0){
				List<String> toList = Arrays.asList(emails.split(";"));
				if(toList != null && !toList.isEmpty()) {
					StringBuilder sb = new StringBuilder();
					for(String email : toList) {
						if(isEmail(email)) {
							sb.append(email).append(";");
						}
					}
					if(sb.toString().length() > 0) {
						validEmails = sb.substring(0, sb.lastIndexOf(";"));
					} else {
						validEmails = null;
					}
				}
			}
		}catch (Exception e) {
			validEmails = null;
			logger.info("Exception while validating email address :"+e.toString());
		}
		return validEmails;
	}
	
	/**
	 * Email Address validation.
	 * @param email
	 * @return boolean
	 */
	private static boolean isEmail(String email) {
		boolean status = true;
		try{
			if(email != null && email.trim().length() > 0) {
				matcher = pattern.matcher(email);
				status = matcher.matches();
			}
		}catch (Exception e) {
			status = false;
		}
		return status;
	}
	// DF: 4881 [E]
	
	/**
	 * Prepare mail recipients of address type (TO, CC, BCC)
	 * @param messageHelper - final variable to form Internet recipient address
	 * @param address - recipient address separated by semicolon (;)
	 * @param type - enum type to denote the type of recipient.
	 * @return - MimeMessageHelper
	 */
	private MimeMessageHelper prepareInternetAddress(final MimeMessageHelper messageHelper, String address,
			AddressType type) {
		try {
			InternetAddress[] internetAddress = null;
			if (address != null && address.trim().length() > 0) {
				String addressArr[] = address.replaceAll(";", ",").split(",");
				internetAddress = new InternetAddress[addressArr.length];
				for (int i = 0; i < addressArr.length; i++) {
					internetAddress[i] = new InternetAddress(addressArr[i]);
				}
			}
			if (internetAddress != null && type != null) {
				if(type.equals(AddressType.TO)) {
					messageHelper.setTo(internetAddress);
				} else if(type.equals(AddressType.CC)) {
					messageHelper.setCc(internetAddress);
				} else if(type.equals(AddressType.BCC)) {
					messageHelper.setBcc(internetAddress);
				}
			}
		} catch (Exception e) {
			logger.info("Exception while preparing Internet address :"+e.toString());
		}
		return messageHelper;
	}
	
	/**
	 * Method to prepare the email attachment with various file types (eg. .pdf, .jpj, .xls,..)  
	 * @param messageHelper - include the file attachments to the message helper
	 * @param mailConfigDO - passing mailing information
	 * @return - MimeMessageHelper
	 */
	private MimeMessageHelper prepareMailAttachment(final MimeMessageHelper messageHelper, MailConfigDO mailConfigDO) {
		try {
			if (mailConfigDO.getAttachments() != null && mailConfigDO.getFileNames() != null
					&& mailConfigDO.getFileNames().size() > 0 && mailConfigDO.getAttachments().size() > 0) {
				
				if(mailConfigDO.getFileNames().size() == mailConfigDO.getAttachments().size()) {
					for (Map.Entry<String, String> fileMap : mailConfigDO.getFileNames().entrySet()) {
						
						String fileName = fileMap.getKey();
						String fileType = fileMap.getValue();
						byte[] fileContents = mailConfigDO.getAttachments().get(fileName);
						
						if (fileName != null && fileType != null && fileContents != null) {
							messageHelper.addAttachment(fileName, new ByteArrayResource(fileContents), fileType);
						}
					}
				} else {
					System.out.println("File attachments mismatched with the file names.");
				}
			}
		}catch (Exception e) {
			logger.info("Exception while preparing Internet address :"+e.toString());
		}
		return messageHelper;
	}


	@Override
	public void sendMailByCategory(MailConfigDO mailConfigDO, int emailCategoryId) {
		String mailSubject = null;String mailContent = null;
		try {
			if(emailCategoryId>0){
				mailConfigDAO.getEmailDetails(mailConfigDO, emailCategoryId);
				
				if(emailCategoryId == EMAIL_CAT_NM_CANCELLATION_APPROVED ){
					//Agent Mail Id
					mailConfigDO.setToAddress(mailConfigDO.getAgentMailId());
					mailConfigDO.setMailType(MailType.HTML);
					
					if(mailConfigDO!=null && mailConfigDO.getMailSubject()!=null){
						mailSubject = mailConfigDO.getMailSubject();
						mailSubject = mailSubject.replaceAll("XXXXX", mailConfigDO.getSubClassCode()!=null?mailConfigDO.getSubClassCode():" - " );
						mailSubject = mailSubject.replaceAll("YYYYY", mailConfigDO.getCoverNoteNo()!=null?mailConfigDO.getCoverNoteNo():" - ");
						mailConfigDO.setMailSubject(mailSubject);
					}
					if(mailConfigDO!=null && mailConfigDO.getMailContent()!=null){
						mailContent = mailConfigDO.getMailContent();
						mailContent = mailContent.replaceAll("AAAAA", mailConfigDO.getReferenceNo()!=null?mailConfigDO.getReferenceNo():" - " );
						mailContent = mailContent.replaceAll("LLLLL", mailConfigDO.getCoverNoteNo()!=null?mailConfigDO.getCoverNoteNo():" - " );
						mailContent = mailContent.replaceAll("PPPPP", mailConfigDO.getMainClassDesc()!=null?mailConfigDO.getMainClassDesc():" - " );
						mailContent = mailContent.replaceAll("BBBBB", mailConfigDO.getSubClassDesc()!=null?mailConfigDO.getSubClassDesc():" - " );
						mailContent = mailContent.replaceAll("DDDDD", mailConfigDO.getAgentCode()!=null?mailConfigDO.getAgentCode():" - " );
						mailContent = mailContent.replaceAll("EEEEE", mailConfigDO.getAgentName()!=null?mailConfigDO.getAgentName():" - " );
						mailContent = mailContent.replaceAll("FFFFF", mailConfigDO.getInsuredName()!=null?mailConfigDO.getInsuredName():" - " );
						mailContent = mailContent.replaceAll("GGGGG", (mailConfigDO.getInceptionDate()!=null?mailConfigDO.getInceptionDate():" - ")+ " to "+ (mailConfigDO.getExpiryDate()!=null?mailConfigDO.getExpiryDate():" - "));
						mailContent = mailContent.replaceAll("HHHHH", mailConfigDO.getInsuredMailId()!=null?mailConfigDO.getInsuredMailId():" - " );
						mailContent = mailContent.replaceAll("IIIII", mailConfigDO.getHousePhNo()!=null?mailConfigDO.getHousePhNo():" - " );
						mailContent = mailContent.replaceAll("JJJJJ", mailConfigDO.getOfficePhNo()!=null?mailConfigDO.getOfficePhNo():" - " );
						mailContent = mailContent.replaceAll("KKKKK", mailConfigDO.getMobileNo()!=null?mailConfigDO.getMobileNo():" - " );
						mailContent = mailContent.replaceAll("OOOOO", mailConfigDO.getFaxNo()!=null?mailConfigDO.getFaxNo():" - " );
						mailContent = mailContent.replaceAll("MMMMM", mailConfigDO.getCancelRemarks()!=null?mailConfigDO.getCancelRemarks():" - " );
						mailContent = mailContent.replaceAll("NNNNN", mailConfigDO.getApprovalRemarks()!=null?mailConfigDO.getApprovalRemarks():" - " );
						mailConfigDO.setMailContent(mailContent);
					}
					
				}else if(emailCategoryId == EMAIL_CAT_NM_CANCELLATION_REJECTED){
					//Agent Mail Id
					mailConfigDO.setToAddress(mailConfigDO.getAgentMailId());
					mailConfigDO.setMailType(MailType.HTML);
					
					if(mailConfigDO!=null && mailConfigDO.getMailSubject()!=null){
						mailSubject = mailConfigDO.getMailSubject();
						mailSubject = mailSubject.replaceAll("XXXXX", mailConfigDO.getSubClassCode()!=null?mailConfigDO.getSubClassCode():" - " );
						mailSubject = mailSubject.replaceAll("YYYYY", mailConfigDO.getCoverNoteNo()!=null?mailConfigDO.getCoverNoteNo():" - ");
						mailConfigDO.setMailSubject(mailSubject);
					}
					if(mailConfigDO!=null && mailConfigDO.getMailContent()!=null){
						mailContent = mailConfigDO.getMailContent();
						mailContent = mailContent.replaceAll("AAAAA", mailConfigDO.getReferenceNo()!=null?mailConfigDO.getReferenceNo():" - " );
						mailContent = mailContent.replaceAll("LLLLL", mailConfigDO.getCoverNoteNo()!=null?mailConfigDO.getCoverNoteNo():" - " );
						mailContent = mailContent.replaceAll("PPPPP", mailConfigDO.getMainClassDesc()!=null?mailConfigDO.getMainClassDesc():" - " );
						mailContent = mailContent.replaceAll("BBBBB", mailConfigDO.getSubClassDesc()!=null?mailConfigDO.getSubClassDesc():" - " );
						mailContent = mailContent.replaceAll("DDDDD", mailConfigDO.getAgentCode()!=null?mailConfigDO.getAgentCode():" - " );
						mailContent = mailContent.replaceAll("EEEEE", mailConfigDO.getAgentName()!=null?mailConfigDO.getAgentName():" - " );
						mailContent = mailContent.replaceAll("FFFFF", mailConfigDO.getInsuredName()!=null?mailConfigDO.getInsuredName():" - " );
						mailContent = mailContent.replaceAll("GGGGG", (mailConfigDO.getInceptionDate()!=null?mailConfigDO.getInceptionDate():" - ")+ " to "+ (mailConfigDO.getExpiryDate()!=null?mailConfigDO.getExpiryDate():" - "));
						mailContent = mailContent.replaceAll("HHHHH", mailConfigDO.getInsuredMailId()!=null?mailConfigDO.getInsuredMailId():" - " );
						mailContent = mailContent.replaceAll("IIIII", mailConfigDO.getHousePhNo()!=null?mailConfigDO.getHousePhNo():" - " );
						mailContent = mailContent.replaceAll("JJJJJ", mailConfigDO.getOfficePhNo()!=null?mailConfigDO.getOfficePhNo():" - " );
						mailContent = mailContent.replaceAll("KKKKK", mailConfigDO.getMobileNo()!=null?mailConfigDO.getMobileNo():" - " );
						mailContent = mailContent.replaceAll("OOOOO", mailConfigDO.getFaxNo()!=null?mailConfigDO.getFaxNo():" - " );
						mailContent = mailContent.replaceAll("MMMMM", mailConfigDO.getCancelRemarks()!=null?mailConfigDO.getCancelRemarks():" - " );
						mailContent = mailContent.replaceAll("NNNNN", mailConfigDO.getApprovalRemarks()!=null?mailConfigDO.getApprovalRemarks():" - " );
						mailConfigDO.setMailContent(mailContent);
					}
				}else if(emailCategoryId == EMAIL_CAT_NM_CANCELLATION_ROUTE){
					
					mailConfigDO.setMailType(MailType.HTML);
					
					if(mailConfigDO!=null && mailConfigDO.getMailSubject()!=null){
						mailSubject = mailConfigDO.getMailSubject();
						mailSubject = mailSubject.replaceAll("XXXXX", mailConfigDO.getSubClassCode()!=null?mailConfigDO.getSubClassCode():" - " );
						mailSubject = mailSubject.replaceAll("YYYYY", mailConfigDO.getCoverNoteNo()!=null?mailConfigDO.getCoverNoteNo():" - ");
						mailConfigDO.setMailSubject(mailSubject);
					}
					if(mailConfigDO!=null && mailConfigDO.getMailContent()!=null){
						mailContent = mailConfigDO.getMailContent();
						mailContent = mailContent.replaceAll("AAAAA", mailConfigDO.getReferenceNo()!=null?mailConfigDO.getReferenceNo():" - " );
						mailContent = mailContent.replaceAll("LLLLL", mailConfigDO.getCoverNoteNo()!=null?mailConfigDO.getCoverNoteNo():" - " );
						mailContent = mailContent.replaceAll("PPPPP", mailConfigDO.getMainClassDesc()!=null?mailConfigDO.getMainClassDesc():" - " );
						mailContent = mailContent.replaceAll("BBBBB", mailConfigDO.getSubClassDesc()!=null?mailConfigDO.getSubClassDesc():" - " );
						mailContent = mailContent.replaceAll("DDDDD", mailConfigDO.getAgentCode()!=null?mailConfigDO.getAgentCode():" - " );
						mailContent = mailContent.replaceAll("EEEEE", mailConfigDO.getAgentName()!=null?mailConfigDO.getAgentName():" - " );
						mailContent = mailContent.replaceAll("FFFFF", mailConfigDO.getInsuredName()!=null?mailConfigDO.getInsuredName():" - " );
						mailContent = mailContent.replaceAll("GGGGG", (mailConfigDO.getInceptionDate()!=null?mailConfigDO.getInceptionDate():" - ")+ " to "+ (mailConfigDO.getExpiryDate()!=null?mailConfigDO.getExpiryDate():" - "));
						mailContent = mailContent.replaceAll("HHHHH", mailConfigDO.getInsuredMailId()!=null?mailConfigDO.getInsuredMailId():" - " );
						mailContent = mailContent.replaceAll("IIIII", mailConfigDO.getHousePhNo()!=null?mailConfigDO.getHousePhNo():" - " );
						mailContent = mailContent.replaceAll("JJJJJ", mailConfigDO.getOfficePhNo()!=null?mailConfigDO.getOfficePhNo():" - " );
						mailContent = mailContent.replaceAll("KKKKK", mailConfigDO.getMobileNo()!=null?mailConfigDO.getMobileNo():" - " );
						mailContent = mailContent.replaceAll("OOOOO", mailConfigDO.getFaxNo()!=null?mailConfigDO.getFaxNo():" - " );
						mailContent = mailContent.replaceAll("MMMMM", mailConfigDO.getCancelRemarks()!=null?mailConfigDO.getCancelRemarks():" - " );
						mailConfigDO.setMailContent(mailContent);
					}
				}else if(emailCategoryId == EMAIL_CAT_NM_REFER_RISK_APPROVED || emailCategoryId == EMAIL_CAT_NM_QT_REFER_RISK_APPROVED){
					//Agent Mail Id
					mailConfigDO.setToAddress(mailConfigDO.getAgentMailId());
					mailConfigDO.setMailType(MailType.HTML);
					
					if(mailConfigDO!=null && mailConfigDO.getMailSubject()!=null){
						mailSubject = mailConfigDO.getMailSubject();
						mailSubject = mailSubject.replaceAll("XXXXX", mailConfigDO.getSubClassCode()!=null?mailConfigDO.getSubClassCode():" - " );
						if(emailCategoryId == EMAIL_CAT_NM_REFER_RISK_APPROVED)
							mailSubject = mailSubject.replaceAll("YYYYY", mailConfigDO.getCoverNoteNo()!=null?mailConfigDO.getCoverNoteNo():(mailConfigDO.getReferenceNo()!=null ? mailConfigDO.getReferenceNo() :" - "));
						else
							mailSubject = mailSubject.replaceAll("YYYYY", mailConfigDO.getQuotationNo()!=null?mailConfigDO.getQuotationNo():(mailConfigDO.getReferenceNo()!=null ? mailConfigDO.getReferenceNo() :" - "));
						mailConfigDO.setMailSubject(mailSubject);
					}
					if(mailConfigDO!=null && mailConfigDO.getMailContent()!=null){
						mailContent = mailConfigDO.getMailContent();
						mailContent = mailContent.replaceAll("AAAAA", mailConfigDO.getReferenceNo()!=null?mailConfigDO.getReferenceNo():" - " );
						if(emailCategoryId == EMAIL_CAT_NM_REFER_RISK_APPROVED)
							mailContent = mailContent.replaceAll("LLLLL", mailConfigDO.getCoverNoteNo()!=null?mailConfigDO.getCoverNoteNo():" - " );
						else
							mailContent = mailContent.replaceAll("LLLLL", mailConfigDO.getQuotationNo()!=null?mailConfigDO.getQuotationNo():" - ");
						
						mailContent = mailContent.replaceAll("PPPPP", mailConfigDO.getMainClassDesc()!=null?mailConfigDO.getMainClassDesc():" - " );
						mailContent = mailContent.replaceAll("BBBBB", mailConfigDO.getSubClassDesc()!=null?mailConfigDO.getSubClassDesc():" - " );
						mailContent = mailContent.replaceAll("DDDDD", mailConfigDO.getAgentCode()!=null?mailConfigDO.getAgentCode():" - " );
						mailContent = mailContent.replaceAll("EEEEE", mailConfigDO.getAgentName()!=null?mailConfigDO.getAgentName():" - " );
						mailContent = mailContent.replaceAll("FFFFF", mailConfigDO.getInsuredName()!=null?mailConfigDO.getInsuredName():" - " );
						mailContent = mailContent.replaceAll("GGGGG", (mailConfigDO.getInceptionDate()!=null?mailConfigDO.getInceptionDate():" - ")+ " to "+ (mailConfigDO.getExpiryDate()!=null?mailConfigDO.getExpiryDate():" - "));
						mailContent = mailContent.replaceAll("HHHHH", mailConfigDO.getInsuredMailId()!=null?mailConfigDO.getInsuredMailId():" - " );
						mailContent = mailContent.replaceAll("IIIII", mailConfigDO.getHousePhNo()!=null?mailConfigDO.getHousePhNo():" - " );
						mailContent = mailContent.replaceAll("JJJJJ", mailConfigDO.getOfficePhNo()!=null?mailConfigDO.getOfficePhNo():" - " );
						mailContent = mailContent.replaceAll("KKKKK", mailConfigDO.getMobileNo()!=null?mailConfigDO.getMobileNo():" - " );
						mailContent = mailContent.replaceAll("OOOOO", mailConfigDO.getFaxNo()!=null?mailConfigDO.getFaxNo():" - " );
						mailContent = mailContent.replaceAll("MMMMM", mailConfigDO.getReferRiskRemarks()!=null?mailConfigDO.getReferRiskRemarks():" - " );
						mailContent = mailContent.replaceAll("NNNNN", mailConfigDO.getApprovalRemarks()!=null?mailConfigDO.getApprovalRemarks():" - " );
						mailConfigDO.setMailContent(mailContent);
					}
				}else if(emailCategoryId == EMAIL_CAT_NM_REFER_RISK_REJECTED || emailCategoryId == EMAIL_CAT_NM_QT_REFER_RISK_REJECTED){
					//Agent Mail Id
					mailConfigDO.setToAddress(mailConfigDO.getAgentMailId());
					mailConfigDO.setMailType(MailType.HTML);
					
					if(mailConfigDO!=null && mailConfigDO.getMailSubject()!=null){
						mailSubject = mailConfigDO.getMailSubject();
						mailSubject = mailSubject.replaceAll("XXXXX", mailConfigDO.getSubClassCode()!=null?mailConfigDO.getSubClassCode():" - " );
						if(emailCategoryId == EMAIL_CAT_NM_REFER_RISK_REJECTED)
							mailSubject = mailSubject.replaceAll("YYYYY", mailConfigDO.getCoverNoteNo()!=null?mailConfigDO.getCoverNoteNo():(mailConfigDO.getReferenceNo()!=null ? mailConfigDO.getReferenceNo() :" - "));
						else
							mailSubject = mailSubject.replaceAll("YYYYY", mailConfigDO.getQuotationNo()!=null?mailConfigDO.getQuotationNo():(mailConfigDO.getReferenceNo()!=null ? mailConfigDO.getReferenceNo() :" - "));
						mailConfigDO.setMailSubject(mailSubject);
					}
					if(mailConfigDO!=null && mailConfigDO.getMailContent()!=null){
						mailContent = mailConfigDO.getMailContent();
						mailContent = mailContent.replaceAll("AAAAA", mailConfigDO.getReferenceNo()!=null?mailConfigDO.getReferenceNo():" - " );
						mailContent = mailContent.replaceAll("PPPPP", mailConfigDO.getMainClassDesc()!=null?mailConfigDO.getMainClassDesc():" - " );
						mailContent = mailContent.replaceAll("BBBBB", mailConfigDO.getSubClassDesc()!=null?mailConfigDO.getSubClassDesc():" - " );
						mailContent = mailContent.replaceAll("DDDDD", mailConfigDO.getAgentCode()!=null?mailConfigDO.getAgentCode():" - " );
						mailContent = mailContent.replaceAll("EEEEE", mailConfigDO.getAgentName()!=null?mailConfigDO.getAgentName():" - " );
						mailContent = mailContent.replaceAll("FFFFF", mailConfigDO.getInsuredName()!=null?mailConfigDO.getInsuredName():" - " );
						mailContent = mailContent.replaceAll("GGGGG", (mailConfigDO.getInceptionDate()!=null?mailConfigDO.getInceptionDate():" - ")+ " to "+ (mailConfigDO.getExpiryDate()!=null?mailConfigDO.getExpiryDate():" - "));
						mailContent = mailContent.replaceAll("HHHHH", mailConfigDO.getInsuredMailId()!=null?mailConfigDO.getInsuredMailId():" - " );
						mailContent = mailContent.replaceAll("IIIII", mailConfigDO.getHousePhNo()!=null?mailConfigDO.getHousePhNo():" - " );
						mailContent = mailContent.replaceAll("JJJJJ", mailConfigDO.getOfficePhNo()!=null?mailConfigDO.getOfficePhNo():" - " );
						mailContent = mailContent.replaceAll("KKKKK", mailConfigDO.getMobileNo()!=null?mailConfigDO.getMobileNo():" - " );
						mailContent = mailContent.replaceAll("OOOOO", mailConfigDO.getFaxNo()!=null?mailConfigDO.getFaxNo():" - " );
						mailContent = mailContent.replaceAll("MMMMM", mailConfigDO.getReferRiskRemarks()!=null?mailConfigDO.getReferRiskRemarks():" - " );
						mailContent = mailContent.replaceAll("NNNNN", mailConfigDO.getApprovalRemarks()!=null?mailConfigDO.getApprovalRemarks():" - " );
						mailConfigDO.setMailContent(mailContent);
					}
				}else if(emailCategoryId == EMAIL_CAT_NM_REFER_RISK_ROUTE || emailCategoryId == EMAIL_CAT_NM_QT_REFER_RISK_ROUTE){
					//Agent Mail Id
					mailConfigDO.setToAddress(mailConfigDO.getStaffMailId());
					mailConfigDO.setMailType(MailType.HTML);
					
					if(mailConfigDO!=null && mailConfigDO.getMailSubject()!=null){
						mailSubject = mailConfigDO.getMailSubject();
						mailSubject = mailSubject.replaceAll("XXXXX", mailConfigDO.getSubClassCode()!=null?mailConfigDO.getSubClassCode():" - " );
						if(emailCategoryId == EMAIL_CAT_NM_REFER_RISK_ROUTE)
							mailSubject = mailSubject.replaceAll("YYYYY", mailConfigDO.getCoverNoteNo()!=null?mailConfigDO.getCoverNoteNo():(mailConfigDO.getReferenceNo()!=null ? mailConfigDO.getReferenceNo() :" - "));
						else
							mailSubject = mailSubject.replaceAll("YYYYY", mailConfigDO.getQuotationNo()!=null?mailConfigDO.getQuotationNo():(mailConfigDO.getReferenceNo()!=null ? mailConfigDO.getReferenceNo() :" - "));
						mailConfigDO.setMailSubject(mailSubject);
					}
					if(mailConfigDO!=null && mailConfigDO.getMailContent()!=null){
						mailContent = mailConfigDO.getMailContent();
						mailContent = mailContent.replaceAll("AAAAA", mailConfigDO.getReferenceNo()!=null?mailConfigDO.getReferenceNo():" - " );
						mailContent = mailContent.replaceAll("PPPPP", mailConfigDO.getMainClassDesc()!=null?mailConfigDO.getMainClassDesc():" - " );
						mailContent = mailContent.replaceAll("BBBBB", mailConfigDO.getSubClassDesc()!=null?mailConfigDO.getSubClassDesc():" - " );
						mailContent = mailContent.replaceAll("DDDDD", mailConfigDO.getAgentCode()!=null?mailConfigDO.getAgentCode():" - " );
						mailContent = mailContent.replaceAll("EEEEE", mailConfigDO.getAgentName()!=null?mailConfigDO.getAgentName():" - " );
						mailContent = mailContent.replaceAll("FFFFF", mailConfigDO.getInsuredName()!=null?mailConfigDO.getInsuredName():" - " );
						mailContent = mailContent.replaceAll("GGGGG", (mailConfigDO.getInceptionDate()!=null?mailConfigDO.getInceptionDate():" - ")+ " to "+ (mailConfigDO.getExpiryDate()!=null?mailConfigDO.getExpiryDate():" - "));
						mailContent = mailContent.replaceAll("HHHHH", mailConfigDO.getInsuredMailId()!=null?mailConfigDO.getInsuredMailId():" - " );
						mailContent = mailContent.replaceAll("IIIII", mailConfigDO.getHousePhNo()!=null?mailConfigDO.getHousePhNo():" - " );
						mailContent = mailContent.replaceAll("JJJJJ", mailConfigDO.getOfficePhNo()!=null?mailConfigDO.getOfficePhNo():" - " );
						mailContent = mailContent.replaceAll("KKKKK", mailConfigDO.getMobileNo()!=null?mailConfigDO.getMobileNo():" - " );
						mailContent = mailContent.replaceAll("OOOOO", mailConfigDO.getFaxNo()!=null?mailConfigDO.getFaxNo():" - " );
						mailContent = mailContent.replaceAll("MMMMM", mailConfigDO.getReferRiskRemarks()!=null?mailConfigDO.getReferRiskRemarks():" - " );
						mailConfigDO.setMailContent(mailContent);
					}
				}
			}
			
			sendMail(mailConfigDO);
			
		} catch (Exception e) {
			logger.info("Exception while sending nm-generic mail ... " + e.toString());
		}
	}
	
}
