/***************************************************************************************************************
 * Date			Developer Name			Defect							

 * 20-DEC-2018	Asaithambi				DF: 5032 - Error Captured in the logs
 * 
 ****************************************************************************************************************/

package com.kgisl.nsure.sms.dao;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.InetSocketAddress;
import java.net.Proxy;
import java.net.URL;
import java.net.URLEncoder;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Map;

import org.apache.axis.client.Service;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.stereotype.Repository;

import com.kgisl.nsure.utils.db.constants.UtilitySQL;

import net.sf.json.JSONObject;

@Repository
public class SMSUtility {

	private static final Logger logger = Logger.getLogger("mail");
	
	private static String SMS_URL = "";

	private static NamedParameterJdbcTemplate namedParameterJdbcTemplate;

	@Autowired
	public void setNamedParameterJdbcTemplate(NamedParameterJdbcTemplate namedParameterJdbcTemplate) {
		SMSUtility.namedParameterJdbcTemplate = namedParameterJdbcTemplate;
	}

	private static final String USER_AGENT = "Mozilla/5.0";

	// Since Axis Service and @Service collides in Service Layer, as a temp fix,
	// moved this to new method!
	public static Service getService() {
		Service service = new Service();
		return service;
	}

	// Load the SMS API URL from DB!
	private void SMS_Initiate() {
		try {
			Map<String, String> params = null;
			SMS_URL = namedParameterJdbcTemplate.queryForObject(UtilitySQL.GET_SMS_API_URL, params, String.class);
		} catch (Exception e) {
			logger.info("Exception while initiating SMS." + e.toString());
		}
	}

	/**
	 * Method to send SMS.
	 * 
	 * @param Message
	 *            to be Sent, Mobile Number
	 * @return status boolean
	 */
	public boolean sendSMS(String message, String mobileNumber) {
		SMS_Initiate();
		boolean isSent = false;
		try {
			SMS_URL = SMS_URL.replace("{NUMBER}", mobileNumber).replace("{MESSAGE}",
					URLEncoder.encode(message, "UTF-8"));

			JSONObject object = getProxyDet();
			InetSocketAddress proxyInet = null;
			if (object.containsKey("proxyIP") && object.containsKey("proxyPort") && object.getString("proxyIP") != null
					&& object.getString("proxyPort") != null) {
				proxyInet = new InetSocketAddress(object.getString("proxyIP"),
						Integer.parseInt(object.getString("proxyPort")));
			}
			if (proxyInet != null) {
				Proxy proxy = new Proxy(Proxy.Type.HTTP, proxyInet);
				URL obj = new URL(SMS_URL);
				HttpURLConnection con = (HttpURLConnection) obj.openConnection(proxy);

				// optional default is GET
				con.setRequestMethod("GET");

				// add request header
				con.setRequestProperty("User-Agent", USER_AGENT);
				
				BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()));
				String inputLine;
				StringBuffer response = new StringBuffer();

				while ((inputLine = in.readLine()) != null)
					response.append(inputLine);
				if (response != null
						&& (response.toString().equalsIgnoreCase("0") || Integer.parseInt(response.toString()) > 0))
					isSent = true;
				in.close();
			}
		} catch (NumberFormatException ne) {
			isSent = true;
		} catch (Exception e) {
			logger.info("Exception while sending SMS :" + e.toString());
		}
		return isSent;
	}

	public JSONObject getProxyDet() {
		JSONObject object = null;
		try {
			SqlParameterSource params = null;
			object = namedParameterJdbcTemplate.queryForObject(UtilitySQL.PROXY_DET, params,
					new RowMapper<JSONObject>() {
						@Override
						public JSONObject mapRow(ResultSet rs, int rowNum) throws SQLException {
							JSONObject object = new JSONObject();
							object.put("proxyIP", rs.getString("VCH_PROXY_IP_ADDR"));
							object.put("proxyPort", rs.getString("VCH_PROXY_PORT"));
							return object;
						}
					});
		} catch (EmptyResultDataAccessException empty) {
		} catch (Exception e) {
			logger.info("Exception while reading proxy details from table :" + e.toString());
		}
		return object;
	}

	public String getSMSAPIStatus() {
		String smsStatus = null;
		try {
			Map<String, Integer> params = null;
			smsStatus = namedParameterJdbcTemplate.queryForObject(UtilitySQL.GET_SMS_API_STATUS, params, String.class);
		} catch (EmptyResultDataAccessException empty) {
		} catch (Exception e) {
			logger.info("Exception while reading SMS API status :"+e.toString());
		}
		return smsStatus;
	}

	public int saveSMS(String accountCode, String coverNote, String message, String mobileNumber, int loginId,
			int purposeId) {
		int smsStatus = 0;
		try {
			MapSqlParameterSource params = new MapSqlParameterSource().addValue("accountCode", accountCode)
					.addValue("coverNote", coverNote).addValue("message", message)
					.addValue("mobileNumber", mobileNumber).addValue("loginId", loginId)
					.addValue("purposeId", purposeId);
			smsStatus = namedParameterJdbcTemplate.update(UtilitySQL.SAVE_SMS_CONTENT.toString(), params);
		} catch (Exception e) {
			logger.info("Exception while saving SMS :" + e.toString());
		}
		return smsStatus;
	}
}
