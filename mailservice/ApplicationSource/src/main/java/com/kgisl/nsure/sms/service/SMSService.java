package com.kgisl.nsure.sms.service;

public interface SMSService {

	public void sendSMS(String message, String mobileNumber, String accountCode, int loginId, String coverNote,
			int purposeId);

	public boolean sendSMS(String message, String mobileNumber);

}
