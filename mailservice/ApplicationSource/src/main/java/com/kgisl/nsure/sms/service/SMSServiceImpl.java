package com.kgisl.nsure.sms.service;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.kgisl.nsure.sms.dao.SMSUtility;

import net.sf.json.JSONObject;

@Service
public class SMSServiceImpl implements SMSService {

	private static final Logger logger = Logger.getLogger("mail");
	
	@Autowired
	private SMSUtility smsUtility; 
	
	@Override
	public void sendSMS(String message, String mobileNumber, String accountCode, int loginId, String coverNote,int purposeId) {
		try {
			logger.info("\n----------- SMS sending process starts -----------");
			if (smsUtility.getSMSAPIStatus().equals("enable") && mobileNumber != null) {
				assignProxyDetails("https");
				logger.info("Sending SMS to "+mobileNumber);
				smsUtility.sendSMS(message, mobileNumber);
				
				logger.info("Saving SMS details to SMS table.");
				smsUtility.saveSMS(accountCode, coverNote, message, mobileNumber, loginId, purposeId);
				logger.info("SMS details saved into SMS table.");
			}
		}catch (Exception e) {
			logger.info("Exception while sending SMS to "+mobileNumber+":"+e.toString());
		}
		logger.info("\n----------- SMS sending process ends -----------");
	}
	
	private void assignProxyDetails(String protocolType){
		JSONObject object = smsUtility.getProxyDet();
		if(object != null) {
			if(protocolType.equals("http")){
				System.setProperty("http.proxyHost", object.containsKey("proxyIP") ? object.getString("proxyIP") : null);
	        	System.setProperty("http.proxyPort", object.containsKey("proxyPort") ? object.getString("proxyPort") : null);
			} else {
				System.setProperty("https.proxyHost", object.containsKey("proxyIP") ? object.getString("proxyIP") : null);
	        	System.setProperty("https.proxyPort", object.containsKey("proxyPort") ? object.getString("proxyPort") : null);
			}
		}
	}

	@Override
	public boolean sendSMS(String message, String mobileNumber) {
		return smsUtility.sendSMS(message, mobileNumber);
	}
	

}
